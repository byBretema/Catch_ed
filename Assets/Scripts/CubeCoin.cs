﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeCoin : MonoBehaviour
{

    float lerpFactor = 50.0f;

    bool oscilleUpRot = false;

    float curRot;
    float minRot = 0.0f;
    float maxRot = 360.0f;

    // Use this for initialization
    void Start()
    {
        curRot = (minRot + maxRot) * 0.5f;
    }

    // Update is called once per frame
    void Update()
    {

        FloatAnimation();

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Blue")
        {
            Audio.I.Play("CubeCoin");
            Destroy(gameObject, 0.1f);
        }
    }

    void FloatAnimation()
    {

        //Oscille rotation
        if (curRot > minRot && !oscilleUpRot)
        {
            curRot -= lerpFactor * Time.deltaTime;
        }
        else
        {
            oscilleUpRot = true;
            curRot += lerpFactor * Time.deltaTime;
        }
        if (curRot > maxRot) { oscilleUpRot = false; }
        transform.rotation = Quaternion.Euler(
            transform.eulerAngles.x,
            curRot,
            transform.eulerAngles.z);

    }
}
