﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.AI;

public class B : MonoBehaviour
{
    // Objects
    A Ag1;
    A Ag2;
    Anim Anim1;
    Anim Anim2;
    [SerializeField] GameObject AgObj1;
    [SerializeField] GameObject AgObj2;

    [SerializeField] Material ghostpillmaterial;
    [SerializeField] Material ghostnopillmaterial;

    // Data to end screen
    public static int PacDead = 0;
    public static int GhostDead = 0;

    // Time
    float TimeWaiting;
    public static float WaitTime = 3f;
    const float SleepTriggerTime = 5f;

    // Distance
    float WaitDistance;
    const float StopDistance = 1f;
    const float AlertDistance = 3.0f;
    const float ChaseDistance = 1.5f;

    // Speed
    public static float FleeSpeedMultiplier = 1.7f;
    public static float ChaseSpeedMultiplier = 2.0f;

    // Use this for initialization
    void Start()
    {
        Ag1 = AgObj1.gameObject.GetComponent<A>();
        Ag2 = AgObj2.gameObject.GetComponent<A>();
        Anim1 = AgObj1.gameObject.GetComponent<Anim>();
        Anim2 = AgObj2.gameObject.GetComponent<Anim>();
        WaitDistance = ChaseDistance * 4f;
    }

    // Update is called once per frame
    void Update()
    {
        // Animation
        Anim1.Play("Bounce");
        Anim2.Play("Bounce");
        AgObj1.GetComponent<Renderer>().material = ghostnopillmaterial;

        // [] Agent 1

        Vector3 Ag1Pos = AgObj1.transform.position;

        bool Ag1IsHidden = Vector3.Distance(
            Gm.NoYVector(Ag1Pos),
            Gm.NoYVector(Ag1.SafeArea.transform.position)
        ) < StopDistance;

        // [] Agent 2

        bool Ag2Pill = Ag2.Pill;

        Vector3 Ag2Pos = AgObj2.transform.position;

        bool Ag2IsHidden = Vector3.Distance(
            Gm.NoYVector(Ag2Pos),
            Gm.NoYVector(Ag2.SafeArea.transform.position)
        ) < StopDistance;

        // [] OTHERS

        float AgentsDistance = Vector3.Distance(Ag1Pos, Ag2Pos);
        float CollisionFactor = (AgObj1.transform.lossyScale.z +
                                 AgObj2.transform.lossyScale.z) * 0.5f;

        // State switcher
        if (AgentsDistance <= CollisionFactor)
        {
            Audio.I.Play("Dead");

            if (Ag2Pill && !Ag1IsHidden)
            {
                GhostDead += 1;
                AgObj1.transform.position = Ag1.InitPos;
            }
            else if (!Ag2IsHidden && !Ag1IsHidden)
            {
                PacDead += 1;
                AgObj2.transform.position = Ag2.InitPos;
            }
        }

        else if (Ag1IsHidden)
        {
            TimeWaiting += Time.deltaTime;
            AgObj1.GetComponent<Renderer>().material = ghostpillmaterial;

            if (AgentsDistance > WaitDistance &&
                TimeWaiting > WaitTime ||
                !Ag2Pill)
            {
                Ag1.State = Gm.STATE.WANDER;
                Ag2.State = Gm.STATE.WANDER;
                TimeWaiting = 0f;
            }
            else
            {
                Ag1.State = Gm.STATE.SLEEP;
                Ag2.State = Gm.STATE.WANDER;
            }
        }

        else if (Ag2Pill)
        {
            Ag1.State = Gm.STATE.WARD;
            AgObj1.GetComponent<Renderer>().material = ghostpillmaterial;
            Ag2.State = Gm.STATE.CHASE;
        }

        else if (Ag2IsHidden)
        {
            TimeWaiting += Time.deltaTime;

            if (AgentsDistance > WaitDistance && TimeWaiting > WaitTime)
            {
                Ag1.State = Gm.STATE.WANDER;
                Ag2.State = Gm.STATE.WANDER;
                TimeWaiting = 0f;
            }
            else
            {
                Ag2.State = Gm.STATE.SLEEP;
                Ag1.State = Gm.STATE.WANDER;
            }
        }

        else if (AgentsDistance < ChaseDistance)
        {
            var PowerPills = GameObject.FindGameObjectsWithTag("PowerPill");
            if (PowerPills.Length > 0)
            {
                Ag1.State = Gm.STATE.CHASE;
                Ag2.State = Gm.STATE.PILL;
            }
            else
            {
                Ag1.State = Gm.STATE.CHASE;
                Ag2.State = Gm.STATE.WARD;
            }
        }

        else if (AgentsDistance <= AlertDistance)
        {
            Ag1.State = Gm.STATE.ALERT;
            Ag2.State = Gm.STATE.RUN;
        }

        else
        {
            Ag1.State = Gm.STATE.WANDER;
            Ag2.State = Gm.STATE.WANDER;
        }
    }
}
