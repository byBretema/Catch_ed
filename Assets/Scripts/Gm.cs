﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Gm : MonoBehaviour
{

    //
    //----------------------------------------------------------------------
    //	From editor vars
    //----------------------------------------------------------------------
    //

    [SerializeField]
    GameObject CubeCoin1;

    [SerializeField]
    GameObject CubeCoin2;

    [SerializeField]
    GameObject PowerPill;

    public GameObject GameOverPanel;
    public GameObject pdUI;
    public GameObject gdUI;
    public GameObject timerUI;

    //
    //----------------------------------------------------------------------
    //	Backend vars
    //----------------------------------------------------------------------
    //

    float timer = 0.0f;
    int ActiveCamera;
    Camera[] Cameras;
    List<Transform> Waypoints = new List<Transform>();

    // States
    public enum STATE
    {
        RUN,
        PILL,
        WARD,
        SLEEP,
        ALERT,
        CHASE,
        WANDER
    }

    //
    //----------------------------------------------------------------------
    //	Unity Methods
    //----------------------------------------------------------------------
    //

    // Before first frame
    // ...
    void Awake()
    {
        Time.timeScale = 1;
        GameOverPanel.SetActive(false);
        Cameras = (Camera[])Camera.FindObjectsOfType(typeof(Camera));
    }

    // On first frame
    // ...
    void Start()
    {
        // Catch Waypoints ORDERED
        var WaypointsParent = GameObject.FindWithTag("Waypoints").transform;
        for (int i = 0; i < WaypointsParent.childCount; i++)
        {
            Waypoints.Add(WaypointsParent.GetChild(i));
        }

        StartCoroutine(InstantiateCoins());
    }

    // On each frame
    // ...
    void Update()
    {
        KeyListener();

        timer += Time.deltaTime;

        var CubeCoins = GameObject.FindGameObjectsWithTag("CubeCoin");
        if (CubeCoins.Length < 1 && timer > 2)
        {
            Time.timeScale = 0;
            GameOverPanel.SetActive(true);
        }
    }

    private void OnGUI()
    {
        pdUI.GetComponent<Text>().text = B.PacDead.ToString();
        gdUI.GetComponent<Text>().text = B.GhostDead.ToString();

        int minutes = Mathf.FloorToInt(timer / 60F);
        int seconds = Mathf.FloorToInt(timer % 60F);
        string TimeData = string.Format("{0:0}:{1:00}", minutes, seconds);
        timerUI.GetComponent<Text>().text = TimeData;

    }

    //
    //----------------------------------------------------------------------
    //	Helpers Methods
    //----------------------------------------------------------------------
    //

    // To populate the board with cubecoins, wait a few between each
    // Instantiate for get a better behaviour because Random Seed will be
    // different for each cube coin.
    IEnumerator InstantiateCoins()
    {

        for (int i = 0; i < Waypoints.Count; i++)
        {
            int ppp = 10; // powerpill probability = 1:ppp
            int tick = Random.Range(0, ppp);

            if (tick == 0)
                Instantiate(PowerPill,
                    Waypoints[i].position,
                    Quaternion.identity);
            else
                Instantiate(CubeCoin1,
                    Waypoints[i].position,
                    Quaternion.identity);

            yield return new WaitForSeconds(0.05f);
        }
    }

    // Locate here all actions that are triggered by a key,
    // by keyboard, mouse or external controller.
    void KeyListener()
    {
        // The active camera is also which have the higher depth so, to avoid
        // to append cameras to the script and use the "enable" property
        // as usual, I prefer (if depth is not crucial) find cameras and
        // iterate over all modifying the depth of the active
        // camera (set to 0) and non-active (set to -1).
        if (Input.GetKeyDown(KeyCode.C))
        {
            // Increment value used to iterate
            ActiveCamera =
                (ActiveCamera < Cameras.Length - 1) ? ActiveCamera + 1 : 0;
            // Switch depth
            for (int i = 0; i < Cameras.Length; i++)
            {
                if (i == ActiveCamera)
                    Cameras[i].depth = 0;
                else
                    Cameras[i].depth = -1;
            }
        } // END: Camera

        if (Input.GetKeyDown(KeyCode.S))
        {
            Time.timeScale = (Time.timeScale == 1) ? 3 : 1;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            ReStart();
        }

        if (Input.GetKeyDown(KeyCode.I))
        {
            if (GameOverPanel.activeInHierarchy == true)
                GameOverPanel.SetActive(false);
            else
                GameOverPanel.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P))
        {
            Time.timeScale = (Time.timeScale == 1) ? 0 : 1;
        }
    }

    public static Vector3 NoYVector(Vector3 v)
    {
        return new Vector3(v.x, 0, v.z);
    }

    public void ReStart()
    {
        SceneManager.LoadScene(0);
    }

    public void Quit()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }

}
