﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.AI;

public class A : MonoBehaviour
{
    // Config

    [Header("Needed")]

    [SerializeField]
    string TargetTag;
    [SerializeField]
    GameObject Enemy;
    [SerializeField]
    string PowerUpTag;
    [SerializeField]
    public GameObject SafeArea;

    [Header("Optionals")]

    [SerializeField]
    string Name;
    [SerializeField]
    float PillDuration = 6f;

    // Behaviour
    bool PillTick;
    NavMeshAgent MyAgent;
    float InitAgentSpeed;
    public Vector3 InitPos { get; private set; }
    public bool Pill { get; private set; }
    private Gm.STATE state;
    public Gm.STATE State
    {
        get
        {
            return state;
        }
        set
        {
            if (state != value)
            {
                if (value == Gm.STATE.ALERT)
                    Audio.I.Play("Alarm");
                state = value;
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        State = Gm.STATE.WANDER;
        MyAgent = GetComponent<NavMeshAgent>();

        InitPos = transform.position;
        InitAgentSpeed = MyAgent.speed;
    }

    // Update is called once per frame
    void Update()
    {
        // Reset values
        MyAgent.enabled = true;
        MyAgent.speed = InitAgentSpeed;

        if (State != Gm.STATE.SLEEP)
        {
            // Behaviour execution
            switch (State)
            {
                case Gm.STATE.PILL:
                    PillTick = true;
                    MyAgent.speed *= B.ChaseSpeedMultiplier;
                    MyAgent.destination = NearPosOf(PowerUpTag);
                    break;

                case Gm.STATE.RUN:
                    MyAgent.speed *= B.FleeSpeedMultiplier;
                    Wander();
                    break;

                case Gm.STATE.WARD:
                    MyAgent.speed *= B.FleeSpeedMultiplier;
                    MyAgent.destination = SafeArea.transform.position;
                    break;

                case Gm.STATE.ALERT:
                    MyAgent.speed *= B.FleeSpeedMultiplier;
                    MyAgent.destination = Enemy.transform.position;
                    break;

                case Gm.STATE.CHASE:
                    MyAgent.speed *= B.ChaseSpeedMultiplier;
                    MyAgent.destination = Enemy.transform.position;
                    break;

                case Gm.STATE.WANDER:
                    Wander();
                    break;

                default:
                    break;
            }
        }

    }

    void OnTriggerEnter(Collider other)
    {
        if (PillTick && other.tag == PowerUpTag)
        {
            Audio.I.Play("PowerPill");
            Destroy(other.gameObject, 0.1f);
            StartCoroutine(PillEffect());
            PillTick = false;
        }
        //if (other.tag == "Blue" || other.tag == "Red")
        //{
        //    Audio.I.Play("Dead");

        //}
    }


    // Powerpill coroutine
    IEnumerator PillEffect()
    {
        Pill = true;
        yield return new WaitForSeconds(PillDuration);
        Pill = false;
    }

    // Wander on the game board
    void Wander()
    {
        var TargetPosAchieved = !MyAgent.pathPending &&
                                MyAgent.remainingDistance <
                                MyAgent.stoppingDistance;

        if (TargetPosAchieved)
            MyAgent.destination = NearPosOf(TargetTag);
    }

    // Near pos
    Vector3 NearPosOf(string target)
    {
        var Targets = GameObject.FindGameObjectsWithTag(target);

        if (Targets.Length <= 0)
            return Vector3.zero;

        if (target == "Waypoint")
        {
            var RandTarget = Random.Range(0, Targets.Length);
            return Targets[RandTarget].transform.position;
        }

        var D = Mathf.Infinity;
        Vector3 OutPos = Vector3.zero;
        foreach (var tgt in Targets)
        {
            var TmpPos = tgt.transform.position;
            var Dist = Vector3.Distance(transform.position, TmpPos);
            if (Dist < D)
            {
                D = Dist;
                OutPos = TmpPos;
            }
        }
        return OutPos;
    }
}
