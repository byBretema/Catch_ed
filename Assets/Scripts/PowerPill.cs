﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerPill : MonoBehaviour {
    float lerpFactor = 0.3f;

    bool oscilleUpHeight = false;
    bool oscilleUpRange = false;

    new Light light;
    float minLightRange = 0.4f;
    float maxLightRange = 0.6f;

    float curHeight;
    float minHeight = -0.2f;
    float maxHeight = 0.5f;

    // -----------------------------------------------------------------------

    private void Start() {
        curHeight = (minHeight + maxHeight) * 0.5f;
        light = GetComponent<Light>();
        light.range = (minLightRange + maxLightRange) * 0.5f;
    }

    // -----------------------------------------------------------------------

    private void Update() {

        FloatAnimation();

    }

    // -----------------------------------------------------------------------

    void FloatAnimation() {

        //Oscille height
        if (curHeight > minHeight && !oscilleUpHeight) {
            curHeight -= lerpFactor * Time.deltaTime;
        } else {
            oscilleUpHeight = true;
            curHeight += lerpFactor * Time.deltaTime;
        }
        if (curHeight > maxHeight) { oscilleUpHeight = false; }
        transform.position = new Vector3(
            transform.position.x,
            curHeight,
            transform.position.z);

        //Oscille light
        if (light.range > minLightRange && !oscilleUpRange) {
            light.range -= lerpFactor * Time.deltaTime;
        } else {
            oscilleUpRange = true;
            light.range += lerpFactor * Time.deltaTime;
        }
        if (light.range > maxLightRange) { oscilleUpRange = false; }

    }

}
